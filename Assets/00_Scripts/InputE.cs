using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputE
{
    private const float deadZone = 0.3f;

    private static Platform platform;
    
    public enum Platform
    {
        Windows,
        OSX,
        Linux
    }

    static InputE()
    {
        if (SystemInfo.operatingSystemFamily == OperatingSystemFamily.Linux)
            platform = Platform.Linux;

        else if (SystemInfo.operatingSystemFamily == OperatingSystemFamily.MacOSX)
            platform = Platform.OSX;

        else
            platform = Platform.Windows;
    }

    public static bool UsingMouse
    {
        get
        {
            return Input.GetMouseButton(0);
        }
    }

    public static float LeftAnalogX
    {
        get
        {
            return Input.GetAxis("Horizontal");
        }
    }

    public static float LeftAnalogY
    {
        get
        {
            return Input.GetAxis("Vertical");
        }
    }

    public static float RightAnalogX
    {
        get
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
                return 1f;
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
                return -1f;
            
            return Input.GetAxis("Horizontal2");
        }
    }

    public static float RightAnalogY
    {
        get
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
                return 1f;
            else if (Input.GetKeyDown(KeyCode.DownArrow))
                return -1f;
                
            return Input.GetAxis("Vertical2");
        }
    }

    public static bool LeftShoulderTrigger
    {
        get
        {
            if (Input.GetKey(KeyCode.F))
                return true;

            if (Input.GetMouseButton(1))
                return true;

            float value = Input.GetAxis("LeftTrigger");
            return value > deadZone;
        }
    }

    public static bool RightShoulderTrigger
    {
        get
        {
            if (Input.GetKey(KeyCode.LeftShift))
                return true;

            if (Input.GetMouseButton(0))
                return true;

            float value = Input.GetAxis("RightTrigger");

            if(platform == Platform.Linux)
                value = Input.GetAxis("RightTriggerLinux");

            return value > deadZone;
        }
    }

    public static bool LeftShoulderButtonDown
    {
        get
        {
            if (Input.GetKeyDown(KeyCode.Q))
                return true;

            if (platform == Platform.Windows)
                return Input.GetKeyDown(KeyCode.Joystick1Button4);
            else if (platform == Platform.OSX)
                return Input.GetKeyDown(KeyCode.Joystick1Button13);
            else
                return Input.GetKeyDown(KeyCode.Joystick1Button4);
        }
    }

    public static bool RightShoulderButtonDown
    {
        get
        {
            if (Input.GetKeyDown(KeyCode.E))
                return true;

            if (platform == Platform.Windows)
                return Input.GetKeyDown(KeyCode.Joystick1Button5);
            else if (platform == Platform.OSX)
                return Input.GetKeyDown(KeyCode.Joystick1Button14);
            else
                return Input.GetKeyDown(KeyCode.Joystick1Button5);
        }
    }

    public static bool AButtonDown
    {
        get
        {
            if (Input.GetKeyDown(KeyCode.Space))
                return true;

            if (platform == Platform.Windows)
                return Input.GetKeyDown(KeyCode.Joystick1Button0);
            else if (platform == Platform.OSX)
                return Input.GetKeyDown(KeyCode.Joystick1Button16);
            else
                return Input.GetKeyDown(KeyCode.Joystick1Button0);
        }
    }

    public static bool AButton
    {
        get
        {
            if (Input.GetKey(KeyCode.Space))
                return true;

            if (platform == Platform.Windows)
                return Input.GetKey(KeyCode.Joystick1Button0);
            else if (platform == Platform.OSX)
                return Input.GetKey(KeyCode.Joystick1Button16);
            else
                return Input.GetKey(KeyCode.Joystick1Button0);
        }
    }

    public static bool StartButtonDown
    {
        get
        {
            if (Input.GetKeyDown(KeyCode.Return))
                return true;

            if (platform == Platform.Windows)
                return Input.GetKeyDown(KeyCode.Joystick1Button7);
            else if (platform == Platform.OSX)
                return Input.GetKeyDown(KeyCode.Joystick1Button9);
            else
                return Input.GetKeyDown(KeyCode.Joystick1Button7);
        }
    }

    public static bool BackButtonDown
    {
        get
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                return true;

            if (platform == Platform.Windows)
                return Input.GetKeyDown(KeyCode.Joystick1Button6);
            else if (platform == Platform.OSX)
                return Input.GetKeyDown(KeyCode.Joystick1Button10);
            else
                return Input.GetKeyDown(KeyCode.Joystick1Button6);
        }
    }
}