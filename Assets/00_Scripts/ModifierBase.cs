﻿using UnityEngine;

public abstract class ModifierBase : MonoBehaviour
{
	[SerializeField]
    private AudioClip doEffectAudioClip = null;

	public void Awake()
	{
		transform.localPosition = Vector3.zero;
	}
	
	public void Start()
	{
		StartCoroutine(Coroutines.ScaleSpawn(transform, new Vector3(1.0f, 1.0f, 1.0f), 0.0f, 0.5f));
	}

	public virtual void DoEffect(Cell cell)
	{
		if(doEffectAudioClip != null)
        {
            AudioSource.PlayClipAtPoint(doEffectAudioClip,MainCamera.Cam.transform.position);
        }

		StopAllCoroutines();
		Destroy(this.gameObject);
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		{
			Cell cell = null;
			if (collision.gameObject.CompareTag("Cell"))
				cell = collision.gameObject.GetComponentInParent<Cell>();
			else if (collision.gameObject.CompareTag("Tentacle"))
				cell = collision.gameObject.GetComponent<Tentacle>().Cell;

			if (cell != null)
				DoEffect(cell);
		}
	}
}