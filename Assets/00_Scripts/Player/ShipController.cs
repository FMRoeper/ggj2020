using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    private readonly int idleBoolParameter = Animator.StringToHash("Idle");
    private readonly int moveBoolParameter = Animator.StringToHash("Move");
    private readonly int viewFloatParameter = Animator.StringToHash("View");
    private readonly int velocityXFloatParameter = Animator.StringToHash("VelocityX");
    private readonly int velocityYFloatParameter = Animator.StringToHash("VelocityY");



    [SerializeField]
    private new AudioSource audio = null;

    [SerializeField]
    private Transform shipTransform = null;

    [SerializeField]
    private Rigidbody2D rb2d = null;

    [SerializeField]
    private Animator shipAnimator = null;

    [SerializeField]
    private float thrustPower = 1f;

    [SerializeField]
    private float boostThrustMultiplier = 2f;
    
    [SerializeField]
    private float slowdownValue = 1f;

    [SerializeField]
    private float maxDefaultVelocity = 5f;

    [SerializeField]
    private float maxBoostVelocity = 10f;

    [SerializeField]
    private float maxVelocitySlowdown = 1f;

    [SerializeField]
    private float deadZone = 0.3f;


    private float maxVelocity;
    private readonly Vector2 v2zero = Vector2.zero;
    public static Vector3 ScreenPos { get; private set; } = Vector3.zero;

    private bool isIdle = true;
    private bool isFlipped = false;
    private Vector2 smoothVelocityVector = Vector2.zero;
    private Vector2 smoothInputVector = Vector2.zero;


    private void Awake()
    {
        shipAnimator.SetBool(idleBoolParameter, true);
    }


    private Vector2 _inputVector = Vector2.zero;
    private Vector3 _rotation = Vector3.zero;
    private void Update()
    {
        ScreenPos = MainCamera.Cam.WorldToScreenPoint(shipTransform.position);
        
        _inputVector.x = InputE.LeftAnalogX;
        _inputVector.y = InputE.LeftAnalogY;

        
        if (_inputVector.magnitude < deadZone)
        {
            _inputVector = v2zero;
            rb2d.velocity = Vector2.MoveTowards(rb2d.velocity, v2zero, slowdownValue * Time.deltaTime);
        }
        else
        {
            rb2d.AddForce(_inputVector * (InputE.AButton ? thrustPower * boostThrustMultiplier : thrustPower) * Time.deltaTime);
            _rotation.z = (Mathf.Atan2(_inputVector.y, _inputVector.x) * Mathf.Rad2Deg) - 90f;
        }

        if (InputE.AButton)
        {
            maxVelocity = maxBoostVelocity;
        }
        else
        {
            maxVelocity = Mathf.MoveTowards(maxVelocity, maxDefaultVelocity, maxVelocitySlowdown * Time.deltaTime);
        }

        rb2d.velocity = Vector2.ClampMagnitude(rb2d.velocity, maxVelocity);
        //shipTransform.localEulerAngles = _rotation;

        smoothInputVector = Vector2.MoveTowards(smoothInputVector, _inputVector.normalized, 1f * Time.deltaTime);
        smoothVelocityVector = Vector2.MoveTowards(smoothVelocityVector, rb2d.velocity.normalized, 5f * Time.deltaTime);
        float velocityMagnitudeAbs = Mathf.Abs(smoothVelocityVector.magnitude);

        if (isIdle)
        {
            if (velocityMagnitudeAbs >= 0.1f)
            {
                isIdle = false;
                shipAnimator.SetBool(idleBoolParameter, false);
                shipAnimator.SetBool(moveBoolParameter, true);
            }
        }
        else
        {
            if (velocityMagnitudeAbs < 0.1f)
            {
                isIdle = true;
                shipAnimator.SetBool(idleBoolParameter, true);
                shipAnimator.SetBool(moveBoolParameter, false);
            }
        }
        if (isFlipped)
        {
            if (smoothInputVector.x > 0.1f)
            {
                isFlipped = false;
            }
        }
        else
        {
            if (smoothInputVector.x < -0.1f)
            {
                
                isFlipped = true;
            }
        }
        
        shipAnimator.SetFloat(velocityXFloatParameter, isFlipped ? smoothVelocityVector.x * -1f : smoothVelocityVector.x);
        shipAnimator.SetFloat(velocityYFloatParameter, smoothVelocityVector.y);

        shipAnimator.SetFloat(viewFloatParameter, Mathf.Abs(smoothInputVector.x));
        shipAnimator.transform.localScale = new Vector3(isFlipped ? -1f : 1f, 1f, 1f);

        audio.volume = Mathf.Clamp(smoothInputVector.magnitude, 0.1f, 0.8f);
    }
}
