using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolManager : MonoBehaviour
{
    [SerializeField]
    private PlayerTool[] tools = null;

    [SerializeField]
    private float inputVectorDeadZone = 0.3f;


    private ToolHud toolHud = null;
    private int currentTool = 0;
    private bool isToolOut = false;

    public static float ToolAngle { get; private set; } = 0f;


    private void Awake()
    {
        toolHud = FindObjectOfType<ToolHud>();
        for (int i = 0; i < tools.Length; i++)
        {
            tools[i].gameObject.SetActive(true);
        }
    }


    private bool _outCheck;
    private Vector2 _inputVector;
    private void Update()
    {
        if (InputE.RightShoulderButtonDown )
        {
            if (isToolOut)
                tools[currentTool].CloseTool();

            currentTool = (currentTool + 1 ) % tools.Length;

            if (isToolOut)
                tools[currentTool].OpenTool();

            toolHud.SwitchTools(tools[currentTool].ToolSprite);
        }
        if (InputE.LeftShoulderButtonDown)
        {
            if (isToolOut)
                tools[currentTool].CloseTool();

            currentTool = (currentTool + tools.Length - 1) % tools.Length;

            if (isToolOut)
                tools[currentTool].OpenTool();

            toolHud.SwitchTools(tools[currentTool].ToolSprite);
        }

        _outCheck = InputE.RightShoulderTrigger;

        if (isToolOut != _outCheck)
        {
            isToolOut = _outCheck;
            if (isToolOut)
            {
                if (!InputE.AButton)
                    tools[currentTool].OpenTool();
            }
            else
                tools[currentTool].CloseTool();
        }

        if (InputE.UsingMouse)
        {
            Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouse.z = 0.0f;

            _inputVector = mouse - transform.position;
        }
        else
        {
            _inputVector.x = InputE.RightAnalogX;
            _inputVector.y = InputE.RightAnalogY;
        }

        if (_inputVector.magnitude > inputVectorDeadZone)
        {
            ToolAngle = (Mathf.Atan2(_inputVector.y, _inputVector.x) * Mathf.Rad2Deg) - 90f;
        }

        if (InputE.AButtonDown)
        {
            if (isToolOut)
                tools[currentTool].CloseTool();
        }
    }
}