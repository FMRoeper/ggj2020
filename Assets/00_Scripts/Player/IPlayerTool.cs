using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerTool
{
    void OpenTool();
    void CloseTool();
}