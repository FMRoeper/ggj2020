using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolHud : MonoBehaviour
{
    [SerializeField]
    private Image spriteImage = null;

    [SerializeField]
    private RectTransform spriteRect = null;

    [SerializeField]
    private float turnSpeed = 0.2f;

    [SerializeField]
    private float showTime = 1f;



    private Vector3 hideScale = new Vector3(0f, 1f, 1f);


    private bool isPlayingRoutine = false;
    private Coroutine routine;


    public void SwitchTools(Sprite sprite)
    {
        spriteImage.gameObject.SetActive(true);
        spriteRect.localScale = hideScale;
        spriteImage.sprite = sprite;

        if (isPlayingRoutine)
            StopCoroutine(routine);
        else
            isPlayingRoutine = true;

        routine = StartCoroutine(Coroutines.LocalScaleLerp(spriteRect, Vector3.one, turnSpeed, Coroutines.LerpType.SmoothOut, () =>
        {
            routine = StartCoroutine(Coroutines.InvokeAction(() => 
            {
                routine = StartCoroutine(Coroutines.LocalScaleLerp(spriteRect, hideScale, turnSpeed, Coroutines.LerpType.SmoothIn, () =>
                {
                    spriteImage.gameObject.SetActive(false);
                    isPlayingRoutine = false;
                }));
            }, showTime));
        }));
    }
}
