using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeSaw : MonoBehaviour, IPlayerTool
{
    [SerializeField]
    private Transform rotateTransform = null;

    [SerializeField]
    private Transform sawTransform = null;

    [SerializeField]
    private LineRenderer lineRenderer = null;

    [SerializeField]
    private float moveOutSpeed = 1f;

    [SerializeField]
    private float moveInSpeed = 1f;

    [SerializeField]
    private Vector3 outScale = new Vector3(0.5f, 0.5f, 0.5f);

    [SerializeField]
    private Vector3 inScale = new Vector3(0.1f, 0.1f, 0.1f);

    [SerializeField]
    private float rotateLerpSpeed = 0.1f;

    [SerializeField]
    private Vector3 sawOutPosition = Vector3.zero;

    [SerializeField]
    private Vector3 sawInPosition = Vector3.zero;
    


    private bool isSawOut = false;
    private Coroutine sawRoutine;

    private bool isPlayingSawRoutine;
    private Coroutine moveSawRoutine;
    private Coroutine scaleSawRoutine;




    private void Start()
    {
        lineRenderer.positionCount = 2;
    }

    public void OpenTool()
    {
        if (!isPlayingSawRoutine)
        {
            rotateTransform.localEulerAngles = new Vector3(0f, 0f, ToolManager.ToolAngle);
        }

        PlayCoroutines(true);
        
        if (!isSawOut)
        {
            sawRoutine = StartCoroutine(SawRoutine());
        }
    }

    public void CloseTool()
    {
        if (isSawOut)
        {
            StopCoroutine(sawRoutine);
        }

        PlayCoroutines(false);
    }

    private void PlayCoroutines(bool isSawOut)
    {
        if (isPlayingSawRoutine)
        {
            StopCoroutine(moveSawRoutine);
            StopCoroutine(scaleSawRoutine);
        }
        else
            isPlayingSawRoutine = true;

        moveSawRoutine = StartCoroutine(Coroutines.LocalPositionLerp(sawTransform, isSawOut ? sawOutPosition : sawInPosition, isSawOut ? moveOutSpeed : moveInSpeed, Coroutines.LerpType.SmoothOut, () => isPlayingSawRoutine = false));
        scaleSawRoutine = StartCoroutine(Coroutines.LocalScaleLerp(sawTransform, isSawOut ? outScale : inScale, isSawOut ? moveOutSpeed : moveInSpeed, Coroutines.LerpType.SmoothOut));
    }


    private IEnumerator SawRoutine()
    {
        for (; ; )
        {
            rotateTransform.localEulerAngles = new Vector3(0f, 0f, Mathf.LerpAngle(rotateTransform.localEulerAngles.z, ToolManager.ToolAngle, rotateLerpSpeed));

            yield return null;
        }
    }

    
    private Vector3[] _linePositions = new Vector3[2];
    private void Update()
    {
        _linePositions[0] = rotateTransform.position;
        _linePositions[1] = sawTransform.position;
        lineRenderer.SetPositions(_linePositions);
    }
}
