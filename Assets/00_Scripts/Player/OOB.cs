using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OOB : MonoBehaviour
{
    [SerializeField]
    private RectTransform thisRect = null;

    [SerializeField]
    private RectTransform arrowRect = null;

    [SerializeField]
    private GameObject thisObject = null;
    
    [SerializeField]
    private float edgeClamp = 100f;


    private Transform player = null;
    private bool isOOB = false;


    private void Awake()
    {
        player = FindObjectOfType<ShipController>().transform;
        thisObject.SetActive(false);
    }



    private Vector3 _screenPos;
    private bool _oobCheck;
    private void Update()
    {
        _screenPos = ShipController.ScreenPos;
        _oobCheck = _screenPos.x < 0 || _screenPos.x > Screen.width || _screenPos.y < 0 || _screenPos.y > Screen.height;

        if (isOOB != _oobCheck)
        {
            isOOB = _oobCheck;
            thisObject.SetActive(isOOB);
        }
        if (isOOB)
        {
            thisRect.anchoredPosition = new Vector2(
                Mathf.Clamp(_screenPos.x, edgeClamp, Screen.width - edgeClamp),
                Mathf.Clamp(_screenPos.y, edgeClamp, Screen.height - edgeClamp));


            arrowRect.localEulerAngles = new Vector3(0f, 0f, Vector2.SignedAngle(Vector2.up, (Vector2)_screenPos - thisRect.anchoredPosition));
        }
    }
}
