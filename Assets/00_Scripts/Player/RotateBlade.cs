using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBlade : MonoBehaviour
{
    [SerializeField]
    private float rotateSpeed = 10f;


    private Vector3 rotation;

    private void Awake()
    {
        rotation = new Vector3(0f, 0f, rotateSpeed);   
    }

    private void Update()
    {
        transform.Rotate(rotation * Time.deltaTime);
    }
}