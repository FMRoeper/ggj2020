using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Claw : PlayerTool
{
    [Header("Claw")]
    [SerializeField]
    protected float pushStrength = 1f;

    [SerializeField]
    private ClawTrigger trigger = null;


    protected override void PushOut()
    {
        Vector2 force = (toolAnchor.position - rotateAnchor.position).normalized * pushStrength;
        for (int i = 0; i < trigger.RigidBodies.Count; i++)
        {
            trigger.RigidBodies[i].AddForce(force);
        }

        base.PushOut();
    }
}