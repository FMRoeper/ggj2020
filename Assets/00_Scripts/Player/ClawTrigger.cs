using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClawTrigger : MonoBehaviour
{
    private const string compareTag = "Modifier";


    public List<Rigidbody2D> RigidBodies = new List<Rigidbody2D>();


    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag(compareTag))
        {
            RigidBodies.Add(collider.GetComponent<Rigidbody2D>());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Rigidbody2D rb2d = collision.GetComponent<Rigidbody2D>();
        if (RigidBodies.Contains(rb2d))
        {
            RigidBodies.Remove(rb2d);
        }
    }
}
