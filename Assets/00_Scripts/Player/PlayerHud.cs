using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHud : MonoBehaviour
{
    [SerializeField]
    private RectTransform thisRect = null;


    private void Update()
    {
        thisRect.anchoredPosition = ShipController.ScreenPos;
    }
}