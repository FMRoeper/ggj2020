using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTool : MonoBehaviour
{
    [SerializeField]
    private Sprite toolSprite = null;
    public Sprite ToolSprite => toolSprite;

    [Space]
    [SerializeField]
    protected Transform rotateAnchor = null;

    [SerializeField]
    protected Transform toolAnchor = null;

    [SerializeField]
    protected SpriteRenderer armRenderer = null;

    [SerializeField]
    protected new Collider2D collider = null;

    [SerializeField]
    protected new AudioSource audio = null;

    [SerializeField]
    protected AudioClip wooshClip = null;

    [SerializeField]
    protected Animator animator = null;


    [Space]
    [SerializeField]
    protected float moveOutSpeed = 1f;

    [SerializeField]
    protected float moveInSpeed = 1f;

    [SerializeField]
    protected Vector3 outScale = new Vector3(0.5f, 0.5f, 0.5f);

    [SerializeField]
    protected Vector3 inScale = new Vector3(0.1f, 0.1f, 0.1f);

    [SerializeField]
    protected float rotateLerpSpeed = 0.1f;

    [SerializeField]
    protected Vector3 outPosition = Vector3.zero;

    [SerializeField]
    protected Vector3 inPosition = Vector3.zero;

    [Space]
    [SerializeField]
    protected Vector3 pushPosition = Vector3.zero;

    [SerializeField]
    protected float pushTime = 0.1f;

    [SerializeField]
    protected float inVolume = 0f;

    [SerializeField]
    protected float outVolume = 0.4f;

    [SerializeField]
    protected float pushVolume = 0.8f;


    private bool isPushing = false;
    private bool isPushRoutinePlaying = false;
    private Coroutine pushRoutine;


    protected bool isPlayingToolRoutine = false;
    protected Coroutine toolRoutine;

    protected bool isPlayingMoveRoutine = false;
    protected Coroutine moveToolRoutine;
    protected Coroutine scaleToolRoutine;

    protected bool isInvokingCollider = false;
    protected Coroutine colliderInvoke;


    protected bool isLerpingVolume = false;
    protected Coroutine volumeRoutine;


    protected virtual void Start()
    {
        armRenderer.size = new Vector2(0f, armRenderer.size.y);
        toolAnchor.localScale = inScale;
        toolAnchor.localPosition = inPosition;
    }

    public virtual void OpenTool()
    {
        if (audio)
        {
            if (isLerpingVolume)
                StopCoroutine(volumeRoutine);
            else
                isLerpingVolume = true;

            volumeRoutine = StartCoroutine(Coroutines.FloatLerp((volume) => audio.volume = volume, audio.volume, outVolume, 0.1f, Coroutines.LerpType.SmoothOut, () => isLerpingVolume = false));
        }

        if (!isPlayingMoveRoutine)
        {
            rotateAnchor.localEulerAngles = new Vector3(0f, 0f, ToolManager.ToolAngle);
        }

        PlayCoroutines(true);

        if (!isPlayingToolRoutine)
        {
            toolRoutine = StartCoroutine(ToolRoutine());
            isPlayingToolRoutine = true;
        }
        if (!isInvokingCollider)
        {
            colliderInvoke = StartCoroutine(Coroutines.InvokeAction(() => 
            {
                collider.gameObject.SetActive(true);
            }, moveOutSpeed));
            isInvokingCollider = true;
        }

        if (animator)
        {
            animator.SetTrigger("Out");
        }
    }

    public virtual void CloseTool()
    {
        if (audio)
        {
            if (isLerpingVolume)
                StopCoroutine(volumeRoutine);
            else
                isLerpingVolume = true;

            volumeRoutine = StartCoroutine(Coroutines.FloatLerp((volume) => audio.volume = volume, audio.volume, inVolume, 0.1f, Coroutines.LerpType.SmoothOut, () => isLerpingVolume = false));
        }

        if (isPushing)
        {
            StopCoroutine(pushRoutine);
            isPushing = false;
        }

        if (isPlayingToolRoutine)
        {
            StopCoroutine(toolRoutine);
            isPlayingToolRoutine = false;
        }

        if (isInvokingCollider)
        {
            StopCoroutine(colliderInvoke);
            isInvokingCollider = false;
        }
        collider.gameObject.SetActive(false);

        PlayCoroutines(false);
    }

    protected virtual void PlayCoroutines(bool isToolOut)
    {
        if (isPlayingMoveRoutine)
        {
            StopCoroutine(moveToolRoutine);
            StopCoroutine(scaleToolRoutine);
        }
        else
            isPlayingMoveRoutine = true;

        moveToolRoutine = StartCoroutine(Coroutines.LocalPositionLerp(toolAnchor, isToolOut ? outPosition : inPosition, isToolOut ? moveOutSpeed : moveInSpeed, Coroutines.LerpType.SmoothOut, () => isPlayingMoveRoutine = false));
        scaleToolRoutine = StartCoroutine(Coroutines.LocalScaleLerp(toolAnchor, isToolOut ? outScale : inScale, isToolOut ? moveOutSpeed : moveInSpeed, Coroutines.LerpType.SmoothOut));
    }

    
    protected virtual void PushOut()
    {
        if (audio)
        {
            if (isLerpingVolume)
                StopCoroutine(volumeRoutine);
            else
                isLerpingVolume = true;

            volumeRoutine = StartCoroutine(Coroutines.FloatLerp((volume) => audio.volume = volume, audio.volume, pushVolume, 0.1f, Coroutines.LerpType.SmoothOut, () => isLerpingVolume = false));
        }

        AudioSource.PlayClipAtPoint(wooshClip, MainCamera.Cam.transform.position, 1f);

        if (isPushRoutinePlaying)
            StopCoroutine(pushRoutine);
        else
            isPushRoutinePlaying = true;

        isPushing = true;
        pushRoutine = StartCoroutine(Coroutines.LocalPositionLerp(toolAnchor, pushPosition, pushTime, Coroutines.LerpType.SmoothIn, () =>
        {
            isPushRoutinePlaying = false;
        }));

        if (animator)
        {
            animator.SetTrigger("Push");
        }
    }

    protected virtual void PushIn()
    {
        if (audio)
        {
            if (isLerpingVolume)
                StopCoroutine(volumeRoutine);
            else
                isLerpingVolume = true;

            volumeRoutine = StartCoroutine(Coroutines.FloatLerp((volume) => audio.volume = volume, audio.volume, outVolume, 0.1f, Coroutines.LerpType.SmoothOut, () => isLerpingVolume = false));
        }

        if (isPushRoutinePlaying)
            StopCoroutine(pushRoutine);
        else
            isPushRoutinePlaying = true;

        pushRoutine = StartCoroutine(Coroutines.LocalPositionLerp(toolAnchor, outPosition, pushTime, Coroutines.LerpType.SmoothOut, () =>
        {
            isPushRoutinePlaying = false;
            isPushing = false;
        }));

        if (animator)
        {
            animator.SetTrigger("Out");
        }
    }

    protected IEnumerator ToolRoutine()
    {
        for (; ; )
        {
            ToolUpdate();

            yield return null;
        }
    }

    protected virtual void ToolUpdate()
    {
        rotateAnchor.localEulerAngles = new Vector3(0f, 0f, Mathf.LerpAngle(rotateAnchor.localEulerAngles.z, ToolManager.ToolAngle, rotateLerpSpeed));

        if (InputE.LeftShoulderTrigger)
        {
            if (!isPushing)
                PushOut();
        }
        else
        {
            if (isPushing && !isPushRoutinePlaying)
                PushIn();
        }
    }

    
    protected virtual void Update()
    {
        armRenderer.size = new Vector2(Vector2.Distance(toolAnchor.localPosition, Vector2.zero), armRenderer.size.y);
    }
}