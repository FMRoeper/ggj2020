using System.Collections;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private static CameraShake instance;

    private float intensity = 0.0f;

    private Vector3 originalPos;

    private void Awake()
    {
        instance = this;
        originalPos = transform.localPosition;
    }

    [ContextMenu("Shake Light")]
    private void ShakeLight()
    {
        Shake(0.5f);
    }

    [ContextMenu("Shake Medium")]
    private void ShakeMedium()
    {
        Shake(1f);
    }

    [ContextMenu("Shake Heavy")]
    private void ShakeHeavy()
    {
        Shake(2f);
    }

    public static void Shake(float intensity)
    {
        instance.intensity = Mathf.Clamp01(instance.intensity + intensity);
    }

    public void Update()
    {
        float trauma = intensity * intensity;

        Vector3 offset = new Vector3(Noise(1.0f) * trauma * 2f, Noise(10.0f) * trauma * 2f, 0.0f);
        float rot = Noise(100.0f) * trauma * 1.0f;

        transform.localPosition = originalPos + offset;
        transform.localRotation = Quaternion.Euler(0.0f, 0.0f, rot);

        intensity = Mathf.Clamp01(intensity - (0.8f * Time.deltaTime));
    }

    private float Noise(float seed)
    {
        return (Mathf.PerlinNoise(Time.time * 2.0f, seed) * 2.0f - 1.0f);
    }
}