using System;
using System.Collections;
using UnityEngine;

public abstract class EnemyBase : MonoBehaviour
{
	public enum State
	{
		SEEKING,
		ATTACHED,
		BOUNCING,
		WANDERING,
	}

	public event EventHandler OnDeath;

	[SerializeField]
	private float velocityMax = 0.0f;

	[SerializeField]
	private float steeringForce = 0.0f;

	[SerializeField]
	private float steeringForceMax = 0.0f;

	[SerializeField]
	private float bounceStrength = 0.0f;

	[SerializeField]
	private float bounceTime = 0.0f;

	[SerializeField]
	private Animator animator;

	[SerializeField]
	private ParticleSystem TrianglesBig = null;

	[SerializeField]
	private ParticleSystem TrianglesSmall = null;

	[SerializeField]
	private AudioClip deathAudioClip;


	[SerializeField]
	private AudioClip bounceAudioClip;


	[SerializeField]
	private AudioClip attatchAudioClip;

	public Health health = new Health();

	private Tentacle target = null;
	public Tentacle Target => target;

	private Tentacle attached = null;
	public Tentacle Attached => attached;

	private State state = State.SEEKING;

	private new Rigidbody2D rigidbody2D = null;

	private Vector3 perpGizmo = Vector3.zero;

	private void OnDrawGizmos()
	{
		if (target != null)
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawLine(this.transform.position, target.transform.position);
		}
	}

	private void Awake()
	{
		rigidbody2D = GetComponent<Rigidbody2D>();

		health.Setup();
		health.OnHealthZero += OnHealthZero;
	}

	private void Start()
	{
		FindTarget();
	}

	private void OnDestroy()
	{
		if(attached != null)
			attached.OnDeath -= OnAttachedDeath;

		health.OnHealthZero -= OnHealthZero;
		StopAllCoroutines();
	}

	private void FixedUpdate()
	{
		if (state != State.ATTACHED)
		{
			Vector2 steering = Vector2.zero;

			if (state == State.SEEKING && (target == null || target.HasEnemy))
			{
				FindTarget();

				if (target == null)
					state = State.WANDERING;
			}

			if (state == State.SEEKING)
			{
				Vector2 desiredPosition = target.enemyAttatchPoint.position;
				
				Vector2 desiredVelocity = (desiredPosition - (Vector2)transform.position).normalized * velocityMax;
				steering += (desiredVelocity - rigidbody2D.velocity) * steeringForce;

				
			}

			if (state == State.WANDERING)
			{

			}

			if (state == State.SEEKING || state == State.WANDERING)
			{
				steering = Vector2.ClampMagnitude(steering, steeringForceMax);
				rigidbody2D.AddForce(steering);

				transform.up = Vector2.Lerp(transform.up, rigidbody2D.velocity, Time.fixedDeltaTime * 7.0f);
			}
		}
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Tentacle"))
		{
			if (state != State.ATTACHED)
			{
				Tentacle tentacle = collision.gameObject.GetComponentInParent<Tentacle>();

				if (tentacle.HasEnemy && tentacle.AttachedEnemyBase != this)
				{
					Bounce(collision);
				}
				else
				{
					Attach(tentacle);
				}
			}
		}
		else if (collision.gameObject.CompareTag("Cell"))
		{
			Bounce(collision);
		}
		else if (collision.collider.gameObject.tag =="Ship")
		{
			//health.HitPoints--;
			Bounce(collision);
		}
		else if (collision.collider.gameObject.tag == "Weapon")
		{
			health.HitPoints--;
		}
		else if (collision.gameObject.CompareTag("Enemy"))
		{
			//Bounce(collision);
		} else if (collision.gameObject.CompareTag("Shield"))
		{
			if (state == State.ATTACHED)
			{
				BounceOff();
			}
			else
			{
				Bounce(collision);
			}
			
		}
	}

	private void Attach(Tentacle tentacle)
	{
		transform.up = tentacle.transform.position - transform.position;

		ReleaseTarget();
		tentacle.Attach(this);
		attached = tentacle;

		attached.OnDeath += OnAttachedDeath;


		//Coroutines.LocalPositionLerp(this.transform, Vector3.zero, 0.5f, Coroutines.LerpType.SmoothOut);
		transform.position = attached.enemyAttatchPoint.position;

		rigidbody2D.velocity = Vector2.zero;
		rigidbody2D.angularVelocity = 0;
		rigidbody2D.isKinematic = true;

		state = State.ATTACHED;

		if(animator != null)
			animator.SetTrigger("Clamp");

		if(attatchAudioClip != null)
		{
			AudioSource.PlayClipAtPoint(attatchAudioClip,MainCamera.Cam.transform.position);
		}
	}

	private void OnHealthZero(object sender, EventArgs e)
	{
		ReleaseTarget();
		ReleaseAttached();

		OnDeath?.Invoke(this, null);

		TrianglesBig.transform.parent = null;
		TrianglesSmall.transform.parent = null;

		TrianglesBig.Play();
		TrianglesSmall.Play();

		Destroy(TrianglesBig.gameObject, 2.0f);
		Destroy(TrianglesSmall.gameObject, 2.0f);

		CameraShake.Shake(0.3f);

		if(deathAudioClip != null)
		{
            AudioSource source = new GameObject().AddComponent<AudioSource>();
            source.transform.position = MainCamera.Cam.transform.position;
            source.clip = deathAudioClip;
            source.pitch = UnityEngine.Random.Range(0.9f, 1.1f);
            source.Play();
            Destroy(source.gameObject, deathAudioClip.length * 2f);
		}

		Destroy(gameObject);
	}

	private void FindTarget()
	{
		if (gameObject != null && gameObject.activeInHierarchy)
		{
			GameManager gameManager = FindObjectOfType<GameManager>();
			target = gameManager.FindNearestEmptyTentacle(transform.position);

			if (target == null)
				target = gameManager.FindNearestTentacle(transform.position);

			if (target)
				target.targettingEnemy = this;
		}
	}

	private void ReleaseTarget()
	{
		if (target == null)
			return;
	
		target.targettingEnemy = null;
		target = null;
	}

	private void ReleaseAttached()
	{
		if (attached != null)
		{
			attached.AttachedEnemyBase = null;
			attached.OnDeath -= OnAttachedDeath;
			attached = null;
		}
	}

	private void OnAttachedDeath(object sender, EventArgs e)
	{
		if (attached != null)
		{
			ReleaseAttached();

			BounceOff();
			
			if(animator != null)
				animator.SetTrigger("Swim");
		}
	}

	private void Bounce(Collision2D collision)
	{
		rigidbody2D.velocity = Vector2.zero;
		Vector2 bounceForce = collision.contacts[0].normal * bounceStrength;
		rigidbody2D.AddForce(bounceForce);

		StartCoroutine(BounceRoutine());
	}

	public void BounceOff()
	{
		if (rigidbody2D)
		{
			rigidbody2D.isKinematic = false;
			rigidbody2D.velocity = Vector2.zero;
			Vector2 bounceForce = -transform.up * bounceStrength;
			rigidbody2D.AddForce(bounceForce);
		}

		StartCoroutine(BounceRoutine());
	}

	private IEnumerator BounceRoutine()
	{
		state = State.BOUNCING;
		
		if(bounceAudioClip != null)
		{
			AudioSource.PlayClipAtPoint(bounceAudioClip,MainCamera.Cam.transform.position);
		}

		yield return new WaitForSeconds(bounceTime);

		state = State.SEEKING;
	}

	private void OnValidate()
	{
		health.OnValidate();
	}
}
