﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestControlls : MonoBehaviour
{
    private new Rigidbody2D rigidbody2D = null;

    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        Vector3 point = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, Camera.main.nearClipPlane));

        transform.position = Vector3.MoveTowards(transform.position, point, 5 * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        if (rigidbody2D != null)
            rigidbody2D.MovePosition(transform.position);
    }
}
