﻿using System;
using System.Collections;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public event EventHandler OnDie;

    [SerializeField]
    private float size = 1.25f;

    [SerializeField]
    private float wobble = 0.05f;

    [SerializeField]
    private float lifeTime = 0.0f;

    private Coroutine wobbleRoutine = null;

    [SerializeField]
    private EnemyBase[] enemies;

    private Collider2D collider;

    private ContactFilter2D contactFilter;
    private int colliderCount;
    private Rigidbody2D foundBody;

    private void Awake()
    {
        transform.localScale = new Vector3(0, 0, 0);

        collider = GetComponentInChildren<Collider2D>();
        enemies = FindObjectsOfType<EnemyBase>();
        contactFilter = new ContactFilter2D();
        StartCoroutine(SpawnRoutine());
    }
    private Collider2D[] colliders;

    private void Update()
    {
        enemies = FindObjectsOfType<EnemyBase>();
        colliders = new Collider2D[enemies.Length + 10];
        colliderCount = collider.OverlapCollider(contactFilter.NoFilter(), colliders);
        if(colliderCount > 0)
        {
            for(int i = 0; i < colliders.Length; i++)
            {
                if(colliders[i] != null)
                {
                    foundBody = colliders[i].GetComponentInParent<Rigidbody2D>();
                    if(foundBody != null && foundBody.isKinematic)
                    {
                        EnemyBase enemyBase = colliders[i].GetComponentInParent<EnemyBase>();
                        if(enemyBase != null)
                        {
                            enemyBase.health.HitPoints--;
                        }
                    }
                }
            }
        }
    }

    private void OnDestroy()
    {
        if (wobbleRoutine != null)
        {
            StopCoroutine(wobbleRoutine);
            wobbleRoutine = null;
        }
    }

    private IEnumerator SpawnRoutine()
    {
        yield return Coroutines.LocalScaleLerp(this.transform, new Vector3(size, size, size), 0.5f, Coroutines.LerpType.SmoothIn);

        wobbleRoutine = StartCoroutine(WobbleRoutine());

        yield return new WaitForSeconds(lifeTime);

        yield return Coroutines.LocalScaleLerp(this.transform, new Vector3(0, 0, 0), 0.5f, Coroutines.LerpType.SmoothIn);

        OnDie?.Invoke(this, null);
        Destroy(this.gameObject);
    } 

    private IEnumerator WobbleRoutine()
    {
        yield return Coroutines.LocalScaleLerp(this.transform, new Vector3(size + wobble, size + wobble, size + wobble), 2.0f, Coroutines.LerpType.SmoothOut);

        while (true)
        {
            yield return Coroutines.LocalScaleLerp(this.transform, new Vector3(size - wobble, size - wobble, size - wobble), 3.0f, Coroutines.LerpType.Smooth);
            yield return Coroutines.LocalScaleLerp(this.transform, new Vector3(size + wobble, size + wobble, size + wobble), 3.0f, Coroutines.LerpType.Smooth);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            // Do cool effect?
        }
    }
}
