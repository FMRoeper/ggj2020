﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public event System.EventHandler OnGameOver;

    public event System.EventHandler OnGameWon;
    [SerializeField]
    private Wave[] waves;

    private int atWave = 0;

    private Wave currentWave {get{return waves[atWave];}}

    private List<Cell> cells = new List<Cell>();

    private List<EnemyBase> spawnedEnemies = new List<EnemyBase>();

    private List<ModifierBase> modifierBases = new List<ModifierBase>();

    // Start is called before the first frame update
    void Awake()
    {
        cells = FindObjectsOfType<Cell>().ToList();
        for(int i = 0; i < cells.Count; i++ )
        {
            cells[i].OnDeath += OnCellDeath;
        }

        //SpawnWave();
    }

    public void SpawnWave()
    {
        //Clear();
        EnemyBase enemyBase = null;
        Vector3 position = Vector3.zero;
        float radius = 5f;
        float angle = 0;
        for(int i = 0; i < currentWave.Enemies.Length; i++)
        {
            angle = 360f / currentWave.Enemies[i].Count;
            for(int j = 0; j < currentWave.Enemies[i].Count; j++)
            {
                position = currentWave.RandomLocation.position;


                position.x = position.x + radius * Mathf.Sin((angle * j) * Mathf.Deg2Rad); 
                position.y = position.y + radius * Mathf.Cos((angle * j) * Mathf.Deg2Rad); 
                
                enemyBase = GameObject.Instantiate(currentWave.Enemies[i].EnemyPrefab,position,Quaternion.identity);
                enemyBase.OnDeath += OnEnemyDeath;
                spawnedEnemies.Add(enemyBase);
            }
        }
        SpawnModifiers();
    }

    private void SpawnModifiers()
    {
        ModifierBase modifierBase = null;
        Vector3 position = Vector3.zero;

        Collider2D collider = null;
        Collider2D[] colliders = new Collider2D[1];
        ContactFilter2D contactFilter = new ContactFilter2D();
        int colliderCount = 0;
        int randomX = 20;
        int randomY = 10;

        for(int i = 0; i < currentWave.ModifierCount; i++)
        {
            
            position.x = Random.Range(transform.position.x -randomX, transform.position.x + randomX);
            position.y = Random.Range(transform.position.y -randomY, transform.position.y + randomY);
            modifierBase = GameObject.Instantiate(currentWave.RandomModifier,position, Quaternion.identity);
            modifierBases.Add(modifierBase);

            collider = modifierBase.GetComponentInChildren<Collider2D>();
            colliderCount = collider.OverlapCollider(contactFilter.NoFilter(), colliders);

            int tried = 0;
            while(colliderCount > 0)
            { 
                position.x = Random.Range(transform.position.x -randomX, transform.position.x + randomX);
                position.y = Random.Range(transform.position.y -randomY, transform.position.y + randomY);
                modifierBase.transform.position = position;

                colliderCount = collider.OverlapCollider(contactFilter.NoFilter(), colliders);
                    
                tried++;
                if(tried > 900)
                {
                    break;
                }
            }
        }
    }

    private void Clear()
    {
        while(spawnedEnemies.Count != 0)
        {
            if(spawnedEnemies[0] != null)
            {
                Destroy(spawnedEnemies[0].gameObject);
            }
            spawnedEnemies.RemoveAt(0);
        }

        while(modifierBases.Count != 0)
        {
            if(modifierBases[0] != null)
            {
                Destroy(modifierBases[0].gameObject);
            }
            modifierBases.RemoveAt(0);
        }
    }

    private void OnEnemyDeath(object sender, System.EventArgs args)
    {
        EnemyBase enemyBase = sender as EnemyBase;
        enemyBase.OnDeath -= OnEnemyDeath;
        spawnedEnemies.Remove(enemyBase);

        if(spawnedEnemies.Count == 0)
        {
            atWave++;
            if(atWave == waves.Length)
            {
                Win();
            }
            else
            {
                SpawnWave();
            }

        }
    }

    private void OnCellDeath(object sender, System.EventArgs args)
    {
        Cell cell = sender as Cell;
        cell.OnDeath -= OnCellDeath;

        cells.Remove(cell);

        EnemyBase[] enemiesInCell = cell.GetComponentsInChildren<EnemyBase>();
        for(int i = 0; i < enemiesInCell.Length; i++)
        {
            enemiesInCell[i].transform.SetParent(null);
            enemiesInCell[i].BounceOff();
        }
        //Destroy(cell.gameObject);

        if(cells.Count == 0)
        {
            GameOver();
        }
    }

    private void GameOver()
    {
        Debug.Log(" LOSERRR");
        OnGameOver?.Invoke(this,null);
        Destroy(FindObjectOfType<ShipController>()?.gameObject);
    }

    private void Win()
    {
        Debug.Log("OOOO You may live");
        OnGameWon?.Invoke(this,null);
    }

    public Tentacle FindNearestTentacle(Vector3 position)
    {
        List<Cell> cellByDistance  = CellByDistance(position);
        Tentacle found = null;
        for(int i =  0; i < cellByDistance.Count; i++)
        {
            found = cellByDistance[i].FindNearestTentacle(position);

            if(found == null)
                continue;
            else
                break;
        }
        return found;
    }

    public Tentacle FindNearestEmptyTentacle(Vector3 position)
    {
        List<Cell> cellByDistance  = CellByDistance(position);
        Tentacle found = null;
        for(int i =  0; i < cellByDistance.Count; i++)
        {
            found = cellByDistance[i].FindNearestEmptyTentacle(position);

            if(found == null)
                continue;
            else
                break;
        }
        return found;
    }

    public Tentacle FindNearestDamagedTentacle(Vector3 position)
    {
        List<Cell> cellByDistance  = CellByDistance(position);
        Tentacle found = null;
        for(int i =  0; i < cellByDistance.Count; i++)
        {
            found = cellByDistance[i].FindNearestDamagedTentacle(position);

            if(found == null)
                continue;
            else
                break;
        }
        return found;
        
    }

    public List<Cell> CellByDistance(Vector3 position)
    {
        List<Cell> sortedCells = new List<Cell>(this.cells);

        sortedCells = sortedCells.OrderBy(t => Vector3.Distance(t.transform.position, position)).ToList();

        return sortedCells;
    }

    public Cell FindNearestCell(Vector3 position)
    {
        return CellByDistance(position).ElementAtOrDefault(0);
    }
}
