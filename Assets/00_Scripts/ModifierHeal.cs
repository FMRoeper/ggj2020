﻿using UnityEngine;

public class ModifierHeal : ModifierBase
{
	[SerializeField]
	private int healAmount = 1;

	public override void DoEffect(Cell cell)
	{
		cell.Heal(transform.position, healAmount);

		base.DoEffect(cell);
	}
}
