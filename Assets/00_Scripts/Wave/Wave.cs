﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Wave 
{
    [SerializeField]
    private EnemiesInWave[] enemies;

    public EnemiesInWave[] Enemies => enemies;

    [SerializeField]
    private Transform[] spawnLocations;
    public Transform RandomLocation{get{return spawnLocations[Random.Range(0,spawnLocations.Length)];}}

    [SerializeField]
    private int modifierCount;
    public int ModifierCount => modifierCount;
    
    [SerializeField]
    private ModifierBase[] modifierBasesPrefab;

    public ModifierBase RandomModifier {get{return modifierBasesPrefab[Random.Range(0,modifierBasesPrefab.Length)];}}
}

[System.Serializable]
public struct EnemiesInWave
{
    [SerializeField]
    public int Count;

    [SerializeField]
    public EnemyBase EnemyPrefab;

    public EnemiesInWave(int count, EnemyBase enemyPrefab)
    {
        this.Count = count;
        this.EnemyPrefab = enemyPrefab;
    }
}
