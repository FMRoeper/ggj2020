using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Coroutines
{
    private readonly static AnimationCurve smoothCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
    private readonly static AnimationCurve smoothDoubleCurve = AnimationCurve.EaseInOut(0f, 0f, 2f, 2f);
    private readonly static AnimationCurve linearCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);


    public enum LerpType
    {
        Smooth,
        SmoothIn,
        SmoothOut,
        Linear
    }


    public static IEnumerator InvokeAction(System.Action action, float delay, System.Action onComplete = null)
    {
        if (delay > 0)
            yield return new WaitForSeconds(delay);

        action?.Invoke();
        onComplete?.Invoke();
    }


    #region Scale Spawn

    private const float scaleSpawnAnimateTime = 0.3f;
    private const float scaleSpawnBumpValue = 0.1f;

    public static IEnumerator ScaleSpawn(Transform transform, System.Action onComplete = null)
        => ScaleSpawn(transform, transform.localScale, scaleSpawnBumpValue, scaleSpawnAnimateTime, onComplete);

    public static IEnumerator ScaleSpawn(Transform transform, float bumpValue, System.Action onComplete = null)
        => ScaleSpawn(transform, transform.localScale, bumpValue, scaleSpawnAnimateTime, onComplete);

    public static IEnumerator ScaleSpawn(Transform transform, float bumpValue, float animateTime, System.Action onComplete = null)
        => ScaleSpawn(transform, transform.localScale, bumpValue, animateTime, onComplete);

    public static IEnumerator ScaleSpawn(Transform transform, Vector3 endScale, System.Action onComplete = null)
        => ScaleSpawn(transform, endScale, scaleSpawnBumpValue, scaleSpawnAnimateTime, onComplete);

    public static IEnumerator ScaleSpawn(Transform transform, Vector3 endScale, float bumpValue, System.Action onComplete = null)
        => ScaleSpawn(transform, endScale, bumpValue, scaleSpawnAnimateTime, onComplete);

    public static IEnumerator ScaleSpawn(Transform transform, Vector3 endScale, float bumpValue, float animateTime, System.Action onComplete = null)
    {
        Vector3 startScale = Vector3.zero;
        Vector3 bumpScale = endScale * (1f + bumpValue);

        float timer = 0f;
        float firstTime = animateTime * 0.75f;
        float secondTime = animateTime - firstTime;

        AnimationCurve firstCurve = AnimationCurve.EaseInOut(0f, -1f, firstTime * 2f, 1f);
        AnimationCurve secondCurve = AnimationCurve.EaseInOut(0f, 0f, secondTime, 1f);

        while (timer < firstTime)
        {
            timer += Time.deltaTime;
            transform.localScale = Vector3.Lerp(startScale, bumpScale, firstCurve.Evaluate(firstTime + timer));
            yield return null;
        }
        timer = 0f;
        while (timer < secondTime)
        {
            timer += Time.deltaTime;
            transform.localScale = Vector3.Lerp(bumpScale, endScale, secondCurve.Evaluate(timer));
            yield return null;
        }

        transform.localScale = endScale;
        onComplete?.Invoke();
    }

    #endregion


    #region Scale Bump

    public static IEnumerator ScaleBump(Transform transform, float bumpValue, float animateTime, System.Action onComplete = null)
        => ScaleBump(transform, transform.localScale, bumpValue, animateTime, onComplete);

    public static IEnumerator ScaleBump(Transform transform, Vector3 startValue, float bumpValue, float animateTime, System.Action onComplete = null)
    {
        Vector3 endValue = startValue * bumpValue;

        float timer = 0f;
        float waitTime = animateTime * 0.1f;
        animateTime *= 0.45f;

        while (timer < animateTime)
        {
            timer += Time.deltaTime;
            transform.localScale = Vector3.Lerp(startValue, endValue, LerpValue(timer / animateTime, LerpType.Smooth));
            yield return null;
        }
        yield return new WaitForSeconds(waitTime);
        while (timer < animateTime)
        {
            timer += Time.deltaTime;
            transform.localScale = Vector3.Lerp(endValue, startValue, LerpValue(timer / animateTime, LerpType.Smooth));
            yield return null;
        }

        transform.localScale = startValue;
        onComplete?.Invoke();
    }

    #endregion


    #region Lerp Routines

    public static IEnumerator LocalScaleLerp(Transform transform, Vector3 endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => Vector3Lerp((Vector3 vector3) => transform.localScale = vector3, transform.localScale, endValue, lerpTime, lerpType, onComplete);

    public static IEnumerator LocalScaleLerp(Transform transform, Vector3 startValue, Vector3 endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => Vector3Lerp((Vector3 vector3) => transform.localScale = vector3, startValue, endValue, lerpTime, lerpType, onComplete);




    public static IEnumerator LocalPositionLerp(Transform transform, Vector3 endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => Vector3Lerp((Vector3 vector3) => transform.localPosition = vector3, transform.localPosition, endValue, lerpTime, lerpType, onComplete);

    public static IEnumerator LocalPositionLerp(Transform transform, Vector3 startValue, Vector3 endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => Vector3Lerp((Vector3 vector3) => transform.localPosition = vector3, startValue, endValue, lerpTime, lerpType, onComplete);


    public static IEnumerator PositionLerp(Transform transform, Vector3 endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => Vector3Lerp((Vector3 vector3) => transform.position = vector3, transform.position, endValue, lerpTime, lerpType, onComplete);

    public static IEnumerator PositionLerp(Transform transform, Vector3 startValue, Vector3 endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => Vector3Lerp((Vector3 vector3) => transform.position = vector3, startValue, endValue, lerpTime, lerpType, onComplete);


    public static IEnumerator AnchoredPositionLerp(RectTransform rectTransform, Vector2 endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => Vector2Lerp((Vector2 vector2) => rectTransform.anchoredPosition = vector2, rectTransform.anchoredPosition, endValue, lerpTime, lerpType, onComplete);

    public static IEnumerator AnchoredPositionLerp(RectTransform rectTransform, Vector2 startValue, Vector2 endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => Vector2Lerp((Vector2 vector2) => rectTransform.anchoredPosition = vector2, startValue, endValue, lerpTime, lerpType, onComplete);
    




    public static IEnumerator ColorLerp(SpriteRenderer spriteRenderer, Color endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => ColorLerp((Color color) => spriteRenderer.color = color, spriteRenderer.color, endValue, lerpTime, lerpType, onComplete);

    public static IEnumerator ColorLerp(SpriteRenderer spriteRenderer, Color startValue, Color endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => ColorLerp((Color color) => spriteRenderer.color = color, startValue, endValue, lerpTime, lerpType, onComplete);


    public static IEnumerator ColorLerp(Image image, Color endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => ColorLerp((Color color) => image.color = color, image.color, endValue, lerpTime, lerpType, onComplete);

    public static IEnumerator ColorLerp(Image image, Color startValue, Color endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => ColorLerp((Color color) => image.color = color, startValue, endValue, lerpTime, lerpType, onComplete);


    public static IEnumerator ColorLerp(Material material, Color endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => ColorLerp((Color color) => material.color = color, material.color, endValue, lerpTime, lerpType, onComplete);

    public static IEnumerator ColorLerp(Material material, Color startValue, Color endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
        => ColorLerp((Color color) => material.color = color, startValue, endValue, lerpTime, lerpType, onComplete);




    public delegate void FloatChange(float newValue);
    public static IEnumerator FloatLerp(FloatChange onFloatChange, float startValue, float endValue, float lerpTime, LerpType lerpType, System.Action onComplete)
    {
        float timer = 0f;
        while (timer < lerpTime)
        {
            timer += Time.deltaTime;
            onFloatChange?.Invoke(Mathf.Lerp(startValue, endValue, LerpValue(timer / lerpTime, lerpType)));
            yield return null;
        }

        onFloatChange?.Invoke(endValue);
        onComplete?.Invoke();
    }

    public delegate void Vector2Change(Vector2 newValue);
    public static IEnumerator Vector2Lerp(Vector2Change onVector2Change, Vector2 startValue, Vector2 endValue, float lerpTime, LerpType lerpType, System.Action onComplete)
    {
        float timer = 0f;
        while (timer < lerpTime)
        {
            timer += Time.deltaTime;
            onVector2Change?.Invoke(Vector2.Lerp(startValue, endValue, LerpValue(timer / lerpTime, lerpType)));
            yield return null;
        }

        onVector2Change(endValue);
        onComplete?.Invoke();
    }

    public delegate void Vector3Change(Vector3 newValue);
    public static IEnumerator Vector3Lerp(Vector3Change onVector3Change, Vector3 startValue, Vector3 endValue, float lerpTime, LerpType lerpType, System.Action onComplete)
    {
        float timer = 0f;
        while (timer < lerpTime)
        {
            timer += Time.deltaTime;
            onVector3Change?.Invoke(Vector3.Lerp(startValue, endValue, LerpValue(timer / lerpTime, lerpType)));
            yield return null;
        }

        onVector3Change(endValue);
        onComplete?.Invoke();
    }

    public delegate void ColorChange(Color newValue);
    public static IEnumerator ColorLerp(ColorChange onColorChange, Color startValue, Color endValue, float lerpTime, LerpType lerpType, System.Action onComplete = null)
    {
        float timer = 0f;
        while (timer < lerpTime)
        {
            timer += Time.deltaTime;
            onColorChange(Color.Lerp(startValue, endValue, LerpValue(timer / lerpTime, lerpType)));
            yield return null;
        }

        onColorChange(endValue);
        onComplete?.Invoke();
    }


    private static float LerpValue(float lerpValue, LerpType lerpType)
    {
        switch (lerpType)
        {
            case LerpType.Smooth:
                return smoothCurve.Evaluate(lerpValue);

            case LerpType.SmoothIn:
                return smoothDoubleCurve.Evaluate(lerpValue);

            case LerpType.SmoothOut:
                return smoothDoubleCurve.Evaluate(1f + lerpValue) - 1f;

            case LerpType.Linear:
                return linearCurve.Evaluate(lerpValue);
        }

        return lerpValue;
    }

    #endregion
}
