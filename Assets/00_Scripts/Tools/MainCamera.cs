using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class MainCamera
{
    public static Camera Cam { get; private set; }

    static MainCamera()
    {
        UpdateCam();
        SceneManager.activeSceneChanged += UpdateCam;
    }

    private static void UpdateCam(Scene current, Scene next)
    {
        UpdateCam();
    }

    public static void UpdateCam()
    {
        Cam = Camera.main;
    }

    public static void UpdateCam(Camera cam)
    {
        Cam = cam;
    }
}
