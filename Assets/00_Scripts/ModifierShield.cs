﻿public class ModifierShield : ModifierBase
{
	public override void DoEffect(Cell cell)
	{
		cell.ActivateShield();

		base.DoEffect(cell);
	}
}
