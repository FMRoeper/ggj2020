using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField]
    private Sprite[] tutorialSprites = null;

    [SerializeField]
    private Image tutorialImage = null;

    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private Transform[] hideOnStart;

    [SerializeField]
    private Transform gameOverScreen;

    [SerializeField]
    private Transform winScreen;

    private bool started = false;
    private int tutorialIndex = 0;

    public void StartGame()
    {
        gameManager.SpawnWave();

        for(int i = 0; i < hideOnStart.Length; i++)
        {
            hideOnStart[i].gameObject.SetActive(false);
        }

        started = true;
        gameManager.OnGameWon += OnGameWon;
        gameManager.OnGameOver += OnGameOver;

    }
    private void OnGameWon(object sender, System.EventArgs args)
    {
        gameManager.OnGameWon -= OnGameWon;
        gameManager.OnGameOver -= OnGameOver;
        winScreen.gameObject.SetActive(true);
    }

    private void OnGameOver(object sender, System.EventArgs args)
    {
        gameManager.OnGameWon -= OnGameWon;
        gameManager.OnGameOver -= OnGameOver;
        winScreen.gameObject.SetActive(false);
        gameOverScreen.gameObject.SetActive(true);

    }

    private void Update()
    {
        if (InputE.BackButtonDown || (InputE.StartButtonDown && started))
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            
        if (InputE.StartButtonDown && !started)
            StartGame();

        if (InputE.AButtonDown || InputE.RightShoulderButtonDown)
        {
            tutorialIndex = (tutorialIndex + 1) % tutorialSprites.Length;
            tutorialImage.sprite = tutorialSprites[tutorialIndex];
        }
        if (InputE.LeftShoulderButtonDown)
        {
            tutorialIndex = (tutorialIndex + tutorialSprites.Length - 1) % tutorialSprites.Length;
            tutorialImage.sprite = tutorialSprites[tutorialIndex];
        }
    }
}
