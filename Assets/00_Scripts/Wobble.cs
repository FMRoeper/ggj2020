﻿using UnityEngine;

public class Wobble : MonoBehaviour
{
    [SerializeField]
    private float scale = 0.01f;

    [SerializeField]
    private float magnitude = 1.0f;

    private void Update()
    {
        float value = (Mathf.PerlinNoise(Time.time * scale, 1) - 0.5f) * magnitude;
        transform.localRotation = Quaternion.Euler(0, value, 0);
    }
}
