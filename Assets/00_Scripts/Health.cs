﻿using System;
using UnityEngine;

[Serializable]
public class Health
{
	public EventHandler OnDamaged;
	public EventHandler OnHealed;
	public EventHandler OnChanged;
	public EventHandler OnHealthZero;

	[SerializeField]
	private int maxHitPoints = 0;

	private int hitPoints = 0;
	public int HitPoints
	{
		get { return hitPoints; }
		set
		{
			hitPoints = Mathf.Clamp(value, 0, maxHitPoints);

			OnChanged?.Invoke(this, null);

			if (value > 0)
				OnHealed?.Invoke(this, null);
			else if (value < 0)
				OnDamaged?.Invoke(this, null);

			if (hitPoints == 0)
				OnHealthZero?.Invoke(this, null);
		}
	}

	public bool IsFull
	{
		get { return hitPoints == maxHitPoints; }
	}

	public bool IsZero
	{
		get { return hitPoints == 0; }
	}

	public void Setup()
	{
		hitPoints = maxHitPoints;
	}

	public void OnValidate()
	{
		Setup();
	}
}
