﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public event System.EventHandler OnDeath;

    [SerializeField]
    private Tentacle tentaclePrefab = null;

    [SerializeField]
    private int tentacleCount = 10;

    [SerializeField]
    private float radius = 2.96f;

    [SerializeField]
    private Shield shieldPrefab = null;

    [SerializeField]
    private Material deathMaterial;

    [SerializeField]
    private new Renderer renderer;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private ParticleSystem healParticle;

    private Shield shield = null;

    private List<Tentacle> tentacles = new List<Tentacle>();

    [SerializeField]
    private AudioClip deathAudioClip;

    public int Health 
    {
        get
        {
            int health = 0;

            foreach (Tentacle tentacle in tentacles)
                health += tentacle.health.HitPoints;

            return health;
        }
    }

    private void Awake()
    {
        tentacles = GetComponentsInChildren<Tentacle>().ToList();
        SpawnTentacles(transform.position,(radius * transform.localScale.x));
        
        if(animator != null)
            animator.speed = UnityEngine.Random.Range(0.8f,1.2f);
    }

    public void Heal(Vector3 pos, int healAmount)
    {
        Tentacle tentacle = FindNearestDamagedTentacle(pos);

        if (tentacle)
        {
            tentacle.health.HitPoints += healAmount;
            healParticle.Play();
        }   
    }

    public void ActivateShield()
    {
        shield = Instantiate(shieldPrefab, transform);
        shield.OnDie += OnShieldDie;
    }

    private void OnShieldDie(object sender, EventArgs e)
    {
        shield = null;
    }

    private void SpawnTentacles(Vector2 center, float radius)
    {
        float angle = 360.0f / tentacleCount;
        Vector2 position;
        Tentacle tentacle = null;
        for(int i = 0; i < tentacleCount; i++)
        {
            position.x = center.x + radius * Mathf.Sin((angle * i) * Mathf.Deg2Rad); 
            position.y = center.y + radius * Mathf.Cos((angle * i) * Mathf.Deg2Rad); 

            tentacle = GameObject.Instantiate(tentaclePrefab, position, Quaternion.AngleAxis(angle * i, Vector3.back), transform);
            tentacle.Setup(this);

            tentacle.OnDeath += OnTentacleDeath;

            tentacles.Add(tentacle);
            
        }
    }

    private void OnTentacleDeath(object sender, System.EventArgs args)
    {
        Tentacle tentacle = sender as Tentacle;
        
        tentacle.OnDeath -= OnTentacleDeath;
        
        tentacles.Remove(tentacle);

        if(Health <= 0)
        {
            if(deathAudioClip != null)
                AudioSource.PlayClipAtPoint(deathAudioClip,MainCamera.Cam.transform.position);
                
            OnDeath?.Invoke(this,null);
            if(renderer != null)
                renderer.material = deathMaterial;
        }
            
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ship") || other.gameObject.CompareTag("Weapon") ) {
            ImpactNearestTentacle(other.transform.position);
        }
    }

    private void ImpactNearestTentacle(Vector3 position)
    {
        Tentacle nearest = FindNearestTentacle(position);

        if (nearest != null)
            nearest.health.HitPoints--;
    }

    public List<Tentacle> TentaclesByDistance(Vector3 position)
    {
        List<Tentacle> sortedTentacles = new List<Tentacle>(this.tentacles);

        sortedTentacles = sortedTentacles.OrderBy(t => Vector3.Distance(t.transform.position, position)).ToList();

        return sortedTentacles;
    }

    public Tentacle FindNearestTentacle(Vector3 position)
    {
        return TentaclesByDistance(position).ElementAtOrDefault(0);
    }

    public Tentacle FindNearestEmptyTentacle(Vector3 position)
    {
        return TentaclesByDistance(position).Where(x => !x.HasEnemy && !x.IsTargetted).ElementAtOrDefault(0);
    }

    public Tentacle FindNearestDamagedTentacle(Vector3 position)
    {
        return TentaclesByDistance(position).Where(x => !x.health.IsFull).ElementAtOrDefault(0);
    }
}
