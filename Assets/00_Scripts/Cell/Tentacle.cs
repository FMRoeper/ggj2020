﻿using System;
using System.Collections;
using UnityEngine;

public class Tentacle : MonoBehaviour
{
	public event EventHandler OnDeath;

	[SerializeField]
	private int timeToDoDamage = 15;

	[SerializeField]
	public Transform enemyAttatchPoint = null;

	[SerializeField]
	public Health health = new Health();

	[SerializeField]
	private Color attackedColor = Color.red;

	[SerializeField]
	private Color damagedColor = Color.cyan;

	[SerializeField]
	private MeshRenderer meshRenderer = null;

	[SerializeField]
	private Animator animator = null;

	[SerializeField]
	private AudioClip deathAudioClip = null;

	private Cell cell = null;
	public Cell Cell => cell;

	private Color defaultColor = Color.white;

	private EnemyBase attachedEnemy = null;
	public EnemyBase AttachedEnemyBase {
		get { return attachedEnemy; }
		set { attachedEnemy = value; }
	}

	public EnemyBase targettingEnemy = null;

	public bool HasEnemy { get { return attachedEnemy != null; } }
	public bool IsTargetted { get { return targettingEnemy != null; } }

	private Coroutine enemyTimerCoroutine;

	private void Awake()
	{
		defaultColor = meshRenderer.material.color;

		health.Setup();

		health.OnChanged += OnHealthChanged;
		health.OnHealthZero += OnHealthZero;

		animator.speed = UnityEngine.Random.Range(0.8f, 1.2f);
	}

	public void Setup(Cell cell)
	{
		this.cell = cell;
	}

	private void UpdateVisualState()
	{
		if (meshRenderer == null)
			return;

		if (health.IsFull && !HasEnemy)
			meshRenderer.material.color = defaultColor;
		else if (!health.IsFull && !HasEnemy)
			meshRenderer.material.color = damagedColor;
		else
			meshRenderer.material.color = attackedColor;
	}

	private void OnHealthZero(object sender, EventArgs e)
	{
		if (attachedEnemy != null)
		{
			attachedEnemy.transform.SetParent(null);
		}

		OnDeath?.Invoke(this, null);

		if (attachedEnemy != null)
		{
			attachedEnemy.OnDeath -= Enemy_OnRemoved;
		}

		health.OnChanged -= OnHealthChanged;
		health.OnHealthZero -= OnHealthZero;

		if(deathAudioClip != null)
			AudioSource.PlayClipAtPoint(deathAudioClip,MainCamera.Cam.transform.position);

		Destroy(gameObject);
	}

	private void OnHealthChanged(object sender, EventArgs e)
	{
		UpdateVisualState();
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("Modifier"))
		{
			health.HitPoints++;
		}
		else if(other.gameObject.CompareTag("Weapon") ||
				other.gameObject.CompareTag("Ship"))
		{
			health.HitPoints--;
		}
	}

	public void Attach(EnemyBase enemy)
	{
		if (enemyAttatchPoint != null)
		{
			enemy.transform.SetParent(enemyAttatchPoint);
		}

		enemy.OnDeath += Enemy_OnRemoved;

		attachedEnemy = enemy;

		meshRenderer.material.color = attackedColor;

		enemyTimerCoroutine = StartCoroutine(EnemyAttatchedTimer(timeToDoDamage));
	}

	private IEnumerator EnemyAttatchedTimer(float time)
	{
		while (!health.IsZero)
		{
			yield return new WaitForSeconds(time);
			health.HitPoints--;
		}

		enemyTimerCoroutine = null;
	}

	private void Enemy_OnRemoved(object sender, System.EventArgs e)
	{
		if (meshRenderer == null)
			return;

		EnemyBase enemy = sender as EnemyBase;
		enemy.transform.SetParent(null);

		if (enemyTimerCoroutine != null)
		{
			StopCoroutine(enemyTimerCoroutine);
			enemyTimerCoroutine = null;
		}

		attachedEnemy = null;

		UpdateVisualState();

		enemy.OnDeath -= Enemy_OnRemoved;
	}

	private void OnValidate()
	{
		health.OnValidate();
	}
}
